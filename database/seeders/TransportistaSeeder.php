<?php

namespace Database\Seeders;

use App\Models\Transportista;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TransportistaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = new Transportista();
    	$table->nombre = "Andrés";
        $table->apellidos = "Trozado";
        $table->slug = Str::slug($table->nombre . "-". $table->apellidos);
    	$table->fechaPermisoConducir = "2000-01-01";
    	$table->imagen = "transportista_01.jpg";
    	$table->save();


    	$table = new Transportista();
    	$table->nombre = "Enrique";
        $table->apellidos = "Cido";
        $table->slug = Str::slug($table->nombre . "-". $table->apellidos);
    	$table->fechaPermisoConducir = "2000-07-07";
    	$table->imagen = "transportista_03.jpg";
    	$table->save();


    	$table = new Transportista();
    	$table->nombre = "Alberto";
        $table->apellidos = "Mate";
        $table->slug = Str::slug($table->nombre . "-". $table->apellidos);
    	$table->fechaPermisoConducir = "2000-09-11";
    	$table->imagen = "transportista_06.jpg";
    	$table->save();


    	$table = new Transportista();
    	$table->nombre = "Estela";
        $table->apellidos = "Gartija";
        $table->slug = Str::slug($table->nombre . "-". $table->apellidos);
    	$table->fechaPermisoConducir = "2001-05-14";
    	$table->imagen = "transportista_02.jpg";
    	$table->save();


    	$table = new Transportista();
    	$table->nombre = "Elsa";
        $table->apellidos = "Capunta";
        $table->slug = Str::slug($table->nombre . "-". $table->apellidos);
    	$table->fechaPermisoConducir = "2001-02-04";
    	$table->imagen = "transportista_04.jpg";
    	$table->save();


    	$table = new Transportista();
    	$table->nombre = "Elena";
        $table->apellidos = "Nito";
        $table->slug = Str::slug($table->nombre . "-". $table->apellidos);
    	$table->fechaPermisoConducir = "2000-11-15";
    	$table->imagen = "transportista_07.jpg";
    	$table->save();

    	$table = new Transportista();
    	$table->nombre = "Susana";
        $table->apellidos = "Torio";
        $table->slug = Str::slug($table->nombre . "-". $table->apellidos);
    	$table->fechaPermisoConducir = "2001-05-30";
    	$table->imagen = "transportista_05.jpg";
    	$table->save();


    	$table = new Transportista();
    	$table->nombre = "Paca";
        $table->apellidos = "Garte";
        $table->slug = Str::slug($table->nombre . "-". $table->apellidos);
    	$table->fechaPermisoConducir = "2019-03-19";
    	$table->imagen = "transportista_10.jpg";
        $table->save();
        
        $table = new Transportista();
    	$table->nombre = "Aitor";
        $table->apellidos = "Tilla";
        $table->slug = Str::slug($table->nombre . "-". $table->apellidos);
    	$table->fechaPermisoConducir = "1985-05-29";
    	$table->imagen = "transportista_09.jpg";
		$table->save();
		

		$transportistas = Transportista::all();
		foreach($transportistas as $transportista)
		{
			$transportista->empresas()->attach([
				rand(1,5),
				rand(6,10)
			]);
		}

    
    }
}
