<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class TransporteController extends Controller
{
    public function Index()
    {
        $transportista = DB::table('transportista')->get();  
  

    return view('transportistas.index',compact('transportistas'));
    }
    public function Show($id)
    {
        $transportista = DB::table('transportistas')->where("id",$id)->first();
      
	return view('transportistas.show' ,["transportistas"=>$transportista]);
    }
}
